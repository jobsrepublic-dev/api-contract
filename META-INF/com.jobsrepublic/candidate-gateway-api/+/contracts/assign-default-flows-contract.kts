package contracts

import org.springframework.cloud.contract.spec.ContractDsl.Companion.contract

contract {
    description = "Returns an OK response containing flows 9000 and 9001 given a 5-or-more-digit ID"
    request {
        url = url("/candidate-gateway/management/v1/default-flows")
        method = POST
        headers {
            contentType = APPLICATION_JSON
        }
        body = body(mapOf(
            "vacancyId" to value(regex("[1-9]{5}")),
            "companyId" to 231,
            "publisherId" to 11
        ))
        bodyMatchers {
            jsonPath("$.vacancyId", byRegex("[0-9]+"))
            jsonPath("$.companyId", byRegex("231"))
            jsonPath("$.publisherId", byRegex("11"))
        }
    }
    response {
        status = CREATED
        headers {
            contentType = APPLICATION_JSON
        }
        body = body(
            "data" to listOf(
                mapOf(
                    "vacancyId" to fromRequest().body("vacancyId"),
                    "flow" to mapOf("id" to 9000,
                                    "name" to "Default vacancy-apply flow",
                                    "flowTypeSlug" to "vacancy-apply"
                    )
                ),
                mapOf(
                    "vacancyId" to fromRequest().body("vacancyId"),
                    "flow" to mapOf("id" to 9001,
                                    "name" to "Default contact-form flow",
                                    "flowTypeSlug" to "contact-form"
                    )
                )
            )
        )

        bodyMatchers {
            jsonPath("$.data[0].vacancyId", byRegex("[0-9]+"))
            jsonPath("$.data[0].flow.id", byRegex("9000"))
            jsonPath("$.data[0].flow.name", byRegex("Default vacancy-apply flow"))
            jsonPath("$.data[0].flow.flowTypeSlug", byRegex("vacancy-apply"))

            jsonPath("$.data[1].vacancyId", byRegex("[0-9]+"))
            jsonPath("$.data[1].flow.id", byRegex("9001"))
            jsonPath("$.data[1].flow.name", byRegex("Default contact-form flow"))
            jsonPath("$.data[1].flow.flowTypeSlug", byRegex("contact-form"))
        }
    }
}