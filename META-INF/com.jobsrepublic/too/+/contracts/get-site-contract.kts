package contracts.site

import org.springframework.cloud.contract.spec.ContractDsl.Companion.contract

arrayOf(
    contract {
        name = "shouldGetSiteBySlug"
        description = "Should get site by slug. The slug should not start with bad-slug-<number> as this pattern is reserved for 404 responses."
        request {
            url = url(v(
                consumer(regex("\\/management\\/v1\\/sites\\/(?!bad\\-slug\\-\\d+)[\\d\\w\\-]+")),
                producer("/management/v1/sites/site-9000"))
            )
            method = GET
        }
        response {
            status = OK
            headers {
                contentType = APPLICATION_JSON
            }
            body = body(
                mapOf(
                    "data" to mapOf(
                        "id" to anyNumber,
                        "siteSlug" to fromRequest().path(3),
                        "name" to anyNonBlankString,
                        "fromEmail" to anyEmail,
                        "active" to true,
                        "daysExpiredVacancyVisible" to anyNumber
                    ))
            )
            bodyMatchers {
                jsonPath("$.data.id", byRegex("[\\d]+"))
                jsonPath("$.data.siteSlug", byRegex("(?!bad\\-slug\\-\\d+)[\\d\\w\\-]+"))
                jsonPath("$.data.name", byRegex(".+"))
                jsonPath("$.data.fromEmail", byRegex("\\w+@\\w+\\.\\w+"))
                jsonPath("$.data.active", byRegex(anyBoolean.asBooleanType()))
                jsonPath("$.data.daysExpiredVacancyVisible", byRegex("\\d+"))
            }
        }
    },

    contract {
        name = "shouldReturnNotFound_GivenNonExistentSiteSlug"
        description = "Should return not-found error response given a non-existent site slug. " +
                "For consumers, slugs having a pattern of bad-slug-<number> are considered non-existent."
        request {
            url = url(v(
                consumer(regex("\\/management\\/v1\\/sites\\/bad\\-slug\\-\\d+")),
                producer("/management/v1/sites/bad-slug-10000"))
            )
            method = GET
        }
        response {
            status = NOT_FOUND
            headers {
                contentType = APPLICATION_JSON
            }
            body = body(
                mapOf(
                    "error" to mapOf(
                        "httpStatus" to "404",
                        "title" to "Not Found",
                        "detail" to "site with slug ${fromRequest().path(3)} not found"
                    )
                )
            )

            bodyMatchers {
                jsonPath("$.error.httpStatus", byRegex("404"))
                jsonPath("$.error.title", byRegex("Not Found"))
                jsonPath("$.error.detail", byRegex("site with slug bad\\-slug\\-\\d+ not found"))
            }
        }
    }
)
