package contracts.vacancy

import org.springframework.cloud.contract.spec.ContractDsl.Companion.contract

arrayOf(
    contract {
        name = "shouldGetVacancyById"
        description = "Returns a vacancy with generated fields given a 4-or-less-digit ID"
        request {
            url = url(v(
                consumer(regex("\\/management\\/v1\\/vacancies\\/\\d{4}")),
                producer("/management/v1/vacancies/9000"))
            )
            method = GET
        }
        response {
            status = OK
            headers {
                contentType = APPLICATION_JSON
            }

            body = body(
                mapOf(
                    "data" to mapOf(
                        "id" to fromRequest().path(3),
                        "title" to anyNonBlankString,
                        "reference" to anyNonBlankString,
                        "isTopItem" to v(regex("true|false")),
                        "slug" to anyNonBlankString,
                        "companyId" to fromRequest().path(3),
                        "companyName" to anyNonBlankString,
                        "publisherId" to fromRequest().path(3),
                        "creationDate" to anyDateTime,
                        "location" to mapOf(
                            "street" to anyNonBlankString,
                            "houseNumber" to anyNonBlankString,
                            "zipCode" to anyNonBlankString,
                            "city" to anyNonBlankString,
                            "region" to anyNonBlankString,
                            "countryCode" to anyNonBlankString
                        ),
                        "contactPerson" to mapOf(
                            "name" to anyNonBlankString,
                            "company" to anyNonBlankString,
                            "phone" to anyNonBlankString,
                            "email" to anyEmail
                        )
                    ))
            )

            bodyMatchers {
                jsonPath("$.data.id", byRegex(".+"))
                jsonPath("$.data.title", byRegex(".+"))
                jsonPath("$.data.reference", byRegex(".+"))
                jsonPath("$.data.isTopItem", byRegex(anyBoolean.asBooleanType()))
                jsonPath("$.data.slug", byRegex(".+"))
                jsonPath("$.data.companyId", byRegex("[0-9]+"))
                jsonPath("$.data.companyName", byRegex(".+"))
                jsonPath("$.data.publisherId", byRegex(".+"))
                jsonPath("$.data.creationDate", byTimestamp)
                jsonPath("$.data.location.street", byRegex(".+"))
                jsonPath("$.data.location.houseNumber", byRegex(".+"))
                jsonPath("$.data.location.zipCode", byRegex(".+"))
                jsonPath("$.data.location.city", byRegex(".+"))
                jsonPath("$.data.location.region", byRegex(".+"))
                jsonPath("$.data.location.countryCode", byRegex(".+"))
                jsonPath("$.data.contactPerson.name", byRegex(".+"))
                jsonPath("$.data.contactPerson.company", byRegex(".+"))
                jsonPath("$.data.contactPerson.phone", byRegex(".+"))
                jsonPath("$.data.contactPerson.email", byRegex("\\w+@\\w+\\.\\w+"))
            }
        }
    },

    contract {
        name = "shouldReturnNotFound_GivenNonExistentVacancyId"
        description = "Returns a not-found error response given a 5-or-more-digit ID"
        request {
            url = url(v(
                consumer(regex("\\/management\\/v1\\/vacancies\\/\\d{5,}")),
                producer("/management/v1/vacancies/10000"))
            )
            method = GET
        }
        response {
            status = NOT_FOUND
            headers {
                contentType = APPLICATION_JSON
            }
            body = body(
                mapOf(
                    "error" to mapOf(
                        "httpStatus" to "404",
                        "title" to "Not Found",
                        "detail" to "Vacancy with id ${fromRequest().path(3)} not found"
                    )
                )
            )

            bodyMatchers {
                jsonPath("$.error.httpStatus", byRegex("404"))
                jsonPath("$.error.title", byRegex("Not Found"))
                jsonPath("$.error.detail", byRegex("Vacancy with id \\d+ not found"))
            }
        }
    },
)
