package contracts.company

import org.springframework.cloud.contract.spec.ContractDsl.Companion.contract

arrayOf(
    contract {
        name = "shouldGetCompanyId"
        description = "Returns a company with generated fields given a 4-or-less-digit ID"
        request {
            url = url(v(
                consumer(regex("\\/management\\/v1\\/companies\\/\\d{1,4}")),
                producer("/management/v1/companies/9000"))
            )
            method = GET
        }
        response {
            status = OK
            headers {
                contentType = APPLICATION_JSON
            }
            body = body(
                mapOf(
                    "data" to mapOf(
                        "id" to fromRequest().path(3),
                        "name" to anyNonBlankString,
                        "slug" to anyNonBlankString,
                        "location" to mapOf(
                            "street" to anyNonBlankString,
                            "houseNumber" to anyNonBlankString,
                            "zipCode" to anyNonBlankString,
                            "city" to anyNonBlankString,
                            "countryCode" to anyNonBlankString
                        )
                    ))
            )
            bodyMatchers {
                jsonPath("$.data.id", byRegex("[\\d]+"))
                jsonPath("$.data.name", byRegex(".+"))
                jsonPath("$.data.slug", byRegex(".+"))
                jsonPath("$.data.location.street", byRegex(".+"))
                jsonPath("$.data.location.houseNumber", byRegex(".+"))
                jsonPath("$.data.location.zipCode", byRegex(".+"))
                jsonPath("$.data.location.city", byRegex(".+"))
                jsonPath("$.data.location.countryCode", byRegex(".+"))
            }
        }
    },

    contract {
        name = "shouldReturnNotFound_GivenNonExistentCompanyId"
        description = "Returns a not-found error response given a 5-or-more-digit ID"
        request {
            url = url(v(
                consumer(regex("\\/management\\/v1\\/companies\\/\\d{5,}")),
                producer("/management/v1/companies/10000"))
            )
            method = GET
        }
        response {
            status = NOT_FOUND
            headers {
                contentType = APPLICATION_JSON
            }
            body = body(
                mapOf(
                    "error" to mapOf(
                        "httpStatus" to "404",
                        "title" to "Not Found",
                        "detail" to "Company with id ${fromRequest().path(3)} not found"
                    )
                )
            )

            bodyMatchers {
                jsonPath("$.error.httpStatus", byRegex("404"))
                jsonPath("$.error.title", byRegex("Not Found"))
                jsonPath("$.error.detail", byRegex("Company with id \\d+ not found"))
            }
        }
    }
)
